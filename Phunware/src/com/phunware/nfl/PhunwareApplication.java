package com.phunware.nfl;

import com.androidquery.callback.BitmapAjaxCallback;

import android.app.Application;

public class PhunwareApplication extends Application {
	public static final int MAX_IMAGE_CACHED = 40;

	@Override
	public void onCreate() {
		super.onCreate();
		BitmapAjaxCallback.setCacheLimit(MAX_IMAGE_CACHED);
	}

	@Override
	public void onLowMemory() {
		BitmapAjaxCallback.clearCache();
	}
}
