package com.phunware.nfl.adapter;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.phunware.nfl.R;
import com.phunware.nfl.model.Venue;

public class VenueAdapter extends ArrayAdapter<Venue> {
	private Context mContext;
	private ArrayList<Venue> mVenueList;
	private final LayoutInflater mInflater;

	public VenueAdapter(Context context, int resourceID, ArrayList<Venue> list) {
		super(context, resourceID, list);
		mContext = context;
		mVenueList = list;
		mInflater = ((Activity) mContext).getLayoutInflater();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = new ViewHolder();

		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.venue_list_item, parent, false);
			holder.name = (TextView) convertView.findViewById(R.id.tv_venue_name);
			holder.address = (TextView) convertView.findViewById(R.id.tv_venue_address);
			convertView.setTag(holder);
		}
		else {
			holder = (ViewHolder) convertView.getTag();
		}

		Venue v = (Venue) mVenueList.get(position);
		if (v != null) {
			holder.name.setText(v.getName());
			holder.address.setText(v.getAddress());
		}

		return convertView;
	}

	private static class ViewHolder {
		TextView name;
		TextView address;
	}
}
