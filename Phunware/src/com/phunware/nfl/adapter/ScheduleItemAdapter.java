package com.phunware.nfl.adapter;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.phunware.nfl.R;
import com.phunware.nfl.model.ScheduleItem;

public class ScheduleItemAdapter extends ArrayAdapter<ScheduleItem> {
	private Context mContext;
	private ArrayList<ScheduleItem> mScheduleItemList;
	private final LayoutInflater mInflater;

	public ScheduleItemAdapter(Context context, int resource, ArrayList<ScheduleItem> list) {
		super(context, resource, list);
		mContext = context;
		mScheduleItemList = list;
		mInflater = ((Activity) mContext).getLayoutInflater();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = new ViewHolder();

		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.schedule_list_item, parent, false);
			holder.schedule = (TextView) convertView.findViewById(R.id.tv_schedule);
			convertView.setTag(holder);
		}
		else {
			holder = (ViewHolder) convertView.getTag();
		}

		ScheduleItem si = (ScheduleItem) mScheduleItemList.get(position);
		if (si != null) {
			holder.schedule.setText(si.getFullSchedule());
		}

		return convertView;
	}

	private static class ViewHolder {
		TextView schedule;
	}
}
