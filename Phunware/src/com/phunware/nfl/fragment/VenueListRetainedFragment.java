package com.phunware.nfl.fragment;

import java.util.ArrayList;

import com.phunware.nfl.model.Venue;

import android.os.Bundle;
import android.support.v4.app.Fragment;

public class VenueListRetainedFragment extends Fragment {
	public static final String TAG = VenueListRetainedFragment.class.getSimpleName();
	private ArrayList<Venue> mVenueList;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRetainInstance(true);// set this fragment to be retained when its activity is destroyed
	}

	public void setData(ArrayList<Venue> list) {
		mVenueList = list;
	}

	public ArrayList<Venue> getData() {
		return mVenueList;
	}
}
