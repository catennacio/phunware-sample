package com.phunware.nfl.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.ShareActionProvider;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.phunware.nfl.R;
import com.phunware.nfl.adapter.ScheduleItemAdapter;
import com.phunware.nfl.model.Venue;

public class VenueDetailsFragment extends Fragment {
	public final static String TAG = VenueDetailsFragment.class.getSimpleName();
	public final static String VENUE_ARG = "venue";

	private ShareActionProvider mShareActionProvider;

	private ImageView mVenueImage;
	private TextView mVenueName;
	private TextView mVenueFirstAddress;
	private TextView mVenueSecondAddress;
	private ListView mSchedule;
	private ProgressBar mProgressBar;
	private LinearLayout mLinearLayout;
	private Venue mVenue;
	private AQuery mAQ;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		if (savedInstanceState != null) {
			mVenue = (Venue) savedInstanceState.getParcelable(VENUE_ARG);
		}

		setHasOptionsMenu(true);
		return inflater.inflate(R.layout.fragment_venue_details, container, false);
	}

	@Override
	public void onStart() {
		super.onStart();

		mAQ = new AQuery(this.getActivity());
		mLinearLayout = (LinearLayout) getActivity().findViewById(R.id.ll_fragment_venue_details);
		mVenueImage = (ImageView) getActivity().findViewById(R.id.iv_image);
		mVenueName = (TextView) getActivity().findViewById(R.id.tv_name);
		mVenueFirstAddress = (TextView) getActivity().findViewById(R.id.tv_first_address);
		mVenueSecondAddress = (TextView) getActivity().findViewById(R.id.tv_second_address);
		mSchedule = (ListView) getActivity().findViewById(R.id.lv_schedule);
		TextView emptyText = (TextView) getActivity().findViewById(android.R.id.empty);
		mSchedule.setEmptyView(emptyText);
		mProgressBar = (ProgressBar) getActivity().findViewById(R.id.progressBar1);

		Bundle args = getArguments();
		if (args != null) {
			mVenue = (Venue) args.getParcelable(VENUE_ARG);
			if (mVenue != null) {
				refreshDetails(mVenue);
			}
		}
		else {
			if (mVenue != null) {
				refreshDetails(mVenue);
			}
		}
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);
		inflater.inflate(R.menu.main_activity_actions, menu);

		MenuItem shareItem = menu.findItem(R.id.main_activity_menu_action_share);
		mShareActionProvider = (ShareActionProvider) MenuItemCompat.getActionProvider(shareItem);
		mShareActionProvider.setShareIntent(this.getShareIntent());
	}

	private Intent getShareIntent() {
		Intent intent = new Intent(Intent.ACTION_SEND);
		intent.setType("text/plain");

		if (mVenue != null) {
			Log.d(TAG, "name=" + mVenue.getName());
			intent.putExtra(android.content.Intent.EXTRA_SUBJECT, mVenue.getName());
			intent.putExtra(android.content.Intent.EXTRA_TITLE, mVenue.getName());
			intent.putExtra(android.content.Intent.EXTRA_TEXT, mVenue.getAddress());
		}

		return intent;
	}

	public void refreshDetails(Venue venue) {
		Log.d(TAG, "selectedId=" + venue.getId());
		mLinearLayout.setVisibility(View.VISIBLE);
		mVenue = venue;
		mVenueName.setText(venue.getName());
		mVenueFirstAddress.setText(venue.getAddress());
		mVenueSecondAddress.setText(venue.getSecondAddress());
		ScheduleItemAdapter adapter = new ScheduleItemAdapter(this.getActivity(), R.id.lv_schedule, venue.getSchedule());
		mSchedule.setAdapter(adapter);

		if (venue.getImageUrl() == null || venue.getImageUrl().equals("")) {
			mVenueImage.setVisibility(View.VISIBLE);
			mProgressBar.setVisibility(View.GONE);
			mVenueImage.setImageResource(R.drawable.img_not_found);
		}
		else {
			// fetch the image from the internet with simple fallback
			mAQ.id(R.id.iv_image).progress(R.id.progressBar2).image(mVenue.getImageUrl(), false, true, 0, R.drawable.img_not_found);
		}
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putParcelable(VENUE_ARG, mVenue);
	}
}
