package com.phunware.nfl.fragment;

import java.util.ArrayList;

import uk.co.senab.actionbarpulltorefresh.library.ActionBarPullToRefresh;
import uk.co.senab.actionbarpulltorefresh.library.Options;
//import uk.co.senab.actionbarpulltorefresh.library.PullToRefreshLayout;
import uk.co.senab.actionbarpulltorefresh.extras.actionbarcompat.PullToRefreshLayout;
import uk.co.senab.actionbarpulltorefresh.library.listeners.OnRefreshListener;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.phunware.nfl.R;
import com.phunware.nfl.adapter.VenueAdapter;
import com.phunware.nfl.model.Venue;
import com.phunware.nfl.network.task.GetVenueListAsync;
import com.phunware.nfl.util.Constants;
import com.phunware.nfl.util.Utils;

public class VenueListFragment extends ListFragment implements OnRefreshListener, GetVenueListAsync.GetVenueListListener {
	public static final String TAG = VenueListFragment.class.getSimpleName();
	private PullToRefreshLayout mPullToRefreshLayout;
	private GetVenueListAsync mGetVenueListAsync;
	private VenueAdapter mAdapter;
	private OnVenueSelectedListener mVenueSelectedCallback;
	private ArrayList<Venue> mVenueList;
	private LinearLayout mErrorLayout;
	private ListView mListView;
	private Button mRefreshButton;
	private ProgressBar mProgressBar;
	private TextView mErrorMsg;

	// Cache list view so we don't reload it on configuration changes
	// (orientation etc)
	// I don't use android:configChanges="orientation" because it's not flexible
	// to retain stateful objects
	// I don't use onSavedInstance because the objects need to be Parcelable,
	// which my arraylist is, but it's not a. fast, b. flexible on design
	// Instead, I use a retained fragment to hold stateful objects. I can put in
	// any objects I want when the Activity/Fragment is re-created when the
	// orientation changes
	private VenueListRetainedFragment mVenueListRetainedFragment;

	public VenueListFragment() {
	}

	public ArrayList<Venue> getVenueList() {
		return mVenueList;
	}

	public interface OnVenueSelectedListener {
		public void onVenueSelected(Venue v);
	}

	@Override
	public void onDestroy() {
		if (mGetVenueListAsync != null && mGetVenueListAsync.getStatus() == AsyncTask.Status.RUNNING) {
			mGetVenueListAsync.cancel(true);
		}

		if (mVenueListRetainedFragment != null) {
			mVenueListRetainedFragment.setData(mVenueList); // save the array list to the retained fragment
		}

		super.onDestroy();
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		mVenueListRetainedFragment = (VenueListRetainedFragment) getActivity().getSupportFragmentManager().findFragmentByTag(VenueListRetainedFragment.TAG);
		// first time, create a retained fragment and start downloading data
		if (mVenueListRetainedFragment == null) {
			mVenueListRetainedFragment = new VenueListRetainedFragment();
			mVenueListRetainedFragment.setTargetFragment(this, 0);
			getActivity().getSupportFragmentManager().beginTransaction().add(mVenueListRetainedFragment, VenueListRetainedFragment.TAG).commit();

			// start downloading the json
			fetchVenueList();
		}
		 // next time, load the data saved in the retained fragment
		else {
			
			mVenueList = mVenueListRetainedFragment.getData();
			mAdapter = new VenueAdapter(getActivity(), R.layout.venue_list_item, mVenueList);
			setListAdapter(mAdapter);
			if (mProgressBar != null)
				mProgressBar.setVisibility(View.GONE);
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View root = inflater.inflate(R.layout.venue_list, null);
		mListView = (ListView) root.findViewById(android.R.id.list);
		mErrorLayout = (LinearLayout) root.findViewById(R.id.ll_error);
		mErrorLayout.setVisibility(View.GONE);
		mRefreshButton = (Button) root.findViewById(R.id.bt_refresh);
		mRefreshButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				fetchVenueList();
			}
		});
		mProgressBar = (ProgressBar) root.findViewById(R.id.progressBar1);
		mProgressBar.setVisibility(View.VISIBLE);
		mErrorMsg = (TextView) root.findViewById(R.id.tv_error_msg);

		return root;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		ViewGroup viewGroup = (ViewGroup) view;

		// some nice pull to refresh
		mPullToRefreshLayout = new PullToRefreshLayout(getActivity());
		ActionBarPullToRefresh.from(getActivity()).insertLayoutInto(viewGroup).theseChildrenArePullable(getListView(), getListView().getEmptyView())
				.listener(this).options(Options.create().scrollDistance(.5f).build()).setup(mPullToRefreshLayout);
	}

	@Override
	public void onRefreshStarted(View view) {
		fetchVenueList();
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);

		try {
			mVenueSelectedCallback = (OnVenueSelectedListener) activity;
		}
		catch (ClassCastException e) {
			throw new ClassCastException(activity.toString() + " must implement OnVenueSelectedListener");
		}
	}

	@Override
	public void onPrepareOptionsMenu(Menu menu) {
		super.onPrepareOptionsMenu(menu);
		MenuItem share = menu.getItem(0);
		share.setVisible(false);
	}

	private void fetchVenueList() {
		Log.d(TAG, "fetchVenueList()");

		if (!Utils.isNetworkAvailable(getActivity())) {
			createNoConnectionDialog().show();
			showErrorLayout();
		}
		else {
			if (mGetVenueListAsync != null && mGetVenueListAsync.getStatus() != AsyncTask.Status.FINISHED) {
				mGetVenueListAsync.cancel(true);
			}
			showLoadingLayout();
			mGetVenueListAsync = new GetVenueListAsync(this);
			mGetVenueListAsync.execute(Constants.FEED_URL);
		}
	}

	private AlertDialog createNoConnectionDialog() {
		return new AlertDialog.Builder(getActivity()).setTitle("No internet connection!").setMessage("Turn on Wifi?")
				.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						Intent intent = new Intent(Settings.ACTION_WIRELESS_SETTINGS);
						startActivity(intent);
					}
				})
				.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
					}
				})
				.setIcon(android.R.drawable.ic_dialog_alert).create();
	}

	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);
		Venue selectedVenue = (Venue) l.getAdapter().getItem(position);
		Log.d(TAG, "onListItemClick() - selectedVenue=" + selectedVenue.getName());
		mVenueSelectedCallback.onVenueSelected(selectedVenue);
	}

	@Override
	public void OnGetVenueListComplete(ArrayList<Venue> list) {
		mVenueList = list;
		mListView.setVisibility(View.VISIBLE);
		mErrorLayout.setVisibility(View.GONE);
		mProgressBar.setVisibility(View.GONE);
		mAdapter = new VenueAdapter(getActivity(), R.layout.venue_list_item, mVenueList);
		setListAdapter(mAdapter);

		if (mPullToRefreshLayout != null && mPullToRefreshLayout.isRefreshing()) {
			mPullToRefreshLayout.setRefreshComplete();
		}
	}

	@Override
	public void OnGetVenueListError(int errCode, String errMsg) {
		Log.e(TAG, "errcode=" + errCode + " errMsg=" + errMsg);
		mErrorMsg.setText("Error: " + errMsg);
		showErrorLayout();
	}

	private void showErrorLayout() {
		if (mPullToRefreshLayout != null && mPullToRefreshLayout.isRefreshing()) {
			mPullToRefreshLayout.setRefreshComplete();
		}
		mListView.setVisibility(View.GONE);
		mErrorLayout.setVisibility(View.VISIBLE);
		mProgressBar.setVisibility(View.GONE);
	}

	private void showLoadingLayout() {
		if (mProgressBar != null)
			mProgressBar.setVisibility(View.VISIBLE);
		if (mListView != null)
			mListView.setVisibility(View.GONE);
		if (mErrorLayout != null)
			mErrorLayout.setVisibility(View.GONE);
	}
}
