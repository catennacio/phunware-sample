package com.phunware.nfl.model;

import java.util.ArrayList;

import android.os.Parcel;
import android.os.Parcelable;

public class Venue implements Parcelable {
	// Core fields
	private long id;
	private int pcode;
	private float latitude;
	private float longitude;
	private String name;
	private String address;
	private String city;
	private String state;
	private String zip;
	private String phone;

	// Super Bowl venue fields
	private String tollfreephone;
	private String mUrl;
	private String description;
	private String ticket_link;
	private String image_url;
	private ArrayList<ScheduleItem> schedule;

	// computed fields
	private float mDistance;

	public Venue() {
	}

	public Venue(Parcel in) {
		readFromParcel(in);
	}

	public static final Parcelable.Creator<Venue> CREATOR = new Parcelable.Creator<Venue>() {
		public Venue createFromParcel(Parcel in) {
			return new Venue(in);
		}

		public Venue[] newArray(int size) {
			return new Venue[size];
		}
	};

	public String getDescription() {
		return description;
	}

	public void setDescription(String d) {
		description = d;
	}

	public String getTicketLink() {
		return ticket_link;
	}

	public void setTicketLink(String ticketLink) {
		ticket_link = ticketLink;
	}

	public ArrayList<ScheduleItem> getSchedule() {
		return schedule;
	}

	public void setSchedule(ArrayList<ScheduleItem> s) {
		schedule = s;
	}

	public String getTollFreePhone() {
		return tollfreephone;
	}

	public void setTollFreePhone(String tollFreePhone) {
		tollfreephone = tollFreePhone;
	}

	public String getUrl() {
		return mUrl;
	}

	public void setUrl(String url) {
		mUrl = url;
	}

	public long getId() {
		return id;
	}

	public void setId(long i) {
		id = i;
	}

	public String getName() {
		return name;
	}

	public void setName(String n) {
		name = n;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String add) {
		address = add;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String ct) {
		city = ct;
	}

	public String getState() {
		return state;
	}

	public void setState(String st) {
		state = st;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String z) {
		zip = z;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String p) {
		phone = p;
	}

	public float getLatitude() {
		return latitude;
	}

	public void setLatitude(float lat) {
		latitude = lat;
	}

	public float getLongitude() {
		return longitude;
	}

	public void setLongitude(float lon) {
		longitude = lon;
	}

	public float getDistance() {
		return mDistance;
	}

	public void setDistance(float distance) {
		mDistance = distance;
	}

	public String getSecondAddress() {
		return city + ", " + state + " " + zip;
	}

	@Override
	public boolean equals(Object o) {
		if (o instanceof Venue && ((Venue) o).getId() == id) {
			return true;
		}
		return false;
	}

	@Override
	public int hashCode() {
		return Long.valueOf(id).hashCode();
	}

	public int getPcode() {
		return pcode;
	}

	public void setPcode(int pc) {
		pcode = pc;
	}

	public String getImageUrl() {
		return image_url;
	}

	public void setImageUrl(String imageUrl) {
		image_url = imageUrl;
	}

	private void readFromParcel(Parcel in) {
		id = in.readLong();
		pcode = in.readInt();
		latitude = in.readFloat();
		longitude = in.readFloat();
		name = in.readString();
		address = in.readString();
		city = in.readString();
		state = in.readString();
		zip = in.readString();
		phone = in.readString();
		tollfreephone = in.readString();
		mUrl = in.readString();
		description = in.readString();
		ticket_link = in.readString();
		image_url = in.readString();
		// in.readList(schedule, null);
		in.readTypedList(schedule, ScheduleItem.CREATOR);
		// mDistance = in.readFloat();
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeLong(id);
		dest.writeInt(pcode);
		dest.writeFloat(latitude);
		dest.writeFloat(longitude);
		dest.writeString(name);
		dest.writeString(address);
		dest.writeString(city);
		dest.writeString(state);
		dest.writeString(zip);
		dest.writeString(phone);
		dest.writeString(tollfreephone);
		dest.writeString(mUrl);
		dest.writeString(description);
		dest.writeString(ticket_link);
		dest.writeString(image_url);
		// dest.writeList(schedule);
		dest.writeTypedList(schedule);
		// dest.writeFloat(mDistance);
	}

}
