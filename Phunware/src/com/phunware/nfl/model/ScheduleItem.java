package com.phunware.nfl.model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.os.Parcel;
import android.os.Parcelable;

public class ScheduleItem implements Parcelable {
	private static final SimpleDateFormat FORMATTER = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss Z");
	private static final SimpleDateFormat STARTDATE_FORMATTER = new SimpleDateFormat("EEEE M/d h:mm a");
	private static final SimpleDateFormat ENDDATE_FORMATTER = new SimpleDateFormat("h:mm a");

	public static final String TAG = ScheduleItem.class.getSimpleName();

	private String start_date;
	private String end_date;

	public String getStart_date() {
		return start_date;
	}

	public void setStart_date(String start_date) {
		this.start_date = start_date;
	}

	public String getEnd_date() {
		return end_date;
	}

	public void setEnd_date(String end_date) {
		this.end_date = end_date;
	}

	private Date mStartDate;
	private Date mEndDate;

	public ScheduleItem() {
	}

	public ScheduleItem(Parcel in) {
		readFromParcel(in);
	}

	public ScheduleItem(Date startDate, Date endDate) {
		mStartDate = startDate;
		mEndDate = endDate;
	}

	public ScheduleItem(String startDate, String endDate) throws ParseException {
		mStartDate = FORMATTER.parse(startDate);
		mEndDate = FORMATTER.parse(endDate);
	}

	public static final Parcelable.Creator<ScheduleItem> CREATOR = new Parcelable.Creator<ScheduleItem>() {
		public ScheduleItem createFromParcel(Parcel in) {
			return new ScheduleItem(in);
		}

		public ScheduleItem[] newArray(int size) {
			return new ScheduleItem[size];
		}
	};

	public Date getStartDate() {
		return mStartDate;
	}

	public void setStartDate(Date startDate) {
		mStartDate = startDate;
	}

	public Date getEndDate() {
		return mEndDate;
	}

	public void setEndDate(Date endDate) {
		mEndDate = endDate;
	}

	public void setStartDate(String startDate) throws ParseException {
		mStartDate = FORMATTER.parse(startDate);
	}

	public void setEndDate(String endDate) throws ParseException {
		mEndDate = FORMATTER.parse(endDate);
	}

	public String getStartDateLocalString() {
		String localStartDate = "";
		try {
			mStartDate = (Date) FORMATTER.parse(start_date);
			localStartDate = STARTDATE_FORMATTER.format(mStartDate);
		}
		catch (ParseException e) {
			e.printStackTrace();
		}
		return localStartDate;
	}

	public String getEndDateLocalString() {
		String localEndDate = "";
		try {
			mEndDate = (Date) FORMATTER.parse(end_date);
			localEndDate = ENDDATE_FORMATTER.format(mEndDate);
		}
		catch (ParseException e) {
			e.printStackTrace();
		}
		return localEndDate;
	}

	public String getFullSchedule() {
		return getStartDateLocalString() + " to " + getEndDateLocalString();
	}

	@Override
	public boolean equals(Object o) {
		boolean result = false;
		if (o instanceof ScheduleItem) {
			result = mStartDate.equals(((ScheduleItem) o).getStartDate()) && mEndDate.equals(((ScheduleItem) o).getEndDate());
		}
		return result;
	}

	@Override
	public int hashCode() {
		String s = getStartDateLocalString() + getEndDateLocalString();
		return s.hashCode();
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		try {
			setStartDate(start_date);
			setEndDate(end_date);
		}
		catch (ParseException e) {
			e.printStackTrace();
		}
		dest.writeLong(mStartDate.getTime());
		dest.writeLong(mEndDate.getTime());
	}

	private void readFromParcel(Parcel in) {
		start_date = in.readString();
		end_date = in.readString();
		try {
			setStartDate(start_date);
			setEndDate(end_date);
		}
		catch (ParseException e) {
			e.printStackTrace();
		}
	}
}
