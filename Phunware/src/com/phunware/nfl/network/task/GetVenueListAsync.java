package com.phunware.nfl.network.task;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.phunware.nfl.model.Venue;

public class GetVenueListAsync extends AsyncTask<String, Integer, ArrayList<Venue>> {
	public static final String TAG = GetVenueListAsync.class.getSimpleName();
	public static final int SUCCESS = 0;
	public static final int ERR_EXCEPTION = -1;
	public static final int ERR_RESPONSE_EMPTY = -2;
	public static final int ERR_CLIENT_PROTOCOL_EXCEPTION = -3;
	public static final int ERR_IO_EXCEPTION = -4;
	public static final int ERR_RUNTIME_EXCEPTION = -5;

	private GetVenueListListener mListener;
	private int mErrCode;
	private String mErrMsg = null;

	public interface GetVenueListListener {
		public void OnGetVenueListComplete(ArrayList<Venue> list);

		public void OnGetVenueListError(int errCode, String errMsg);
	}

	public GetVenueListAsync(GetVenueListListener mListener) {
		super();
		this.mListener = mListener;
	}

	@Override
	protected ArrayList<Venue> doInBackground(String... params) {
		Log.d(TAG, "doInBackground()");
		ArrayList<Venue> list = new ArrayList<Venue>();
		HttpResponse response = null;

		try {
			HttpClient httpclient = new DefaultHttpClient();
			HttpGet httpGet = new HttpGet(params[0]);
			httpGet.addHeader("Accept", "text/xml");
			response = httpclient.execute(httpGet);
			StatusLine statusLine = response.getStatusLine();

			Log.d(TAG, "Response status code=" + response.getStatusLine().getStatusCode());

			if (statusLine.getStatusCode() == HttpStatus.SC_OK) {
				ByteArrayOutputStream out = new ByteArrayOutputStream();
				response.getEntity().writeTo(out);
				out.close();
				String responseString = out.toString();
				// Log.d(TAG, "Response=" + responseString);
				if (responseString == null || responseString.isEmpty()) {
					mErrCode = ERR_RESPONSE_EMPTY;
					return list;
				}
				else {
					Gson gson = new Gson();
					Type listType = new TypeToken<ArrayList<Venue>>() {
					}.getType();
					list = gson.fromJson(responseString, listType);
					mErrCode = SUCCESS;
				}
			}
			else {
				Log.d(TAG, "FAIL - Code=" + response.getStatusLine().getStatusCode() + "\nReason=" + statusLine.getReasonPhrase());
				mErrCode = response.getStatusLine().getStatusCode();
				mErrMsg = statusLine.getReasonPhrase();
			}
		}
		catch (ClientProtocolException e) {
			mErrCode = ERR_CLIENT_PROTOCOL_EXCEPTION;
			mErrMsg = e.getMessage();
			e.printStackTrace();
			Log.e(TAG, "" + e.getMessage());
		}
		catch (IOException e) {
			mErrCode = ERR_IO_EXCEPTION;
			mErrMsg = e.getMessage();
			e.printStackTrace();
			Log.e(TAG, "" + e.getMessage());
		}
		catch (RuntimeException e) {
			mErrCode = ERR_RUNTIME_EXCEPTION;
			mErrMsg = e.getMessage();
			e.printStackTrace();
			Log.e(TAG, "" + e.getMessage());
		}
		catch (Exception e) {
			mErrCode = ERR_EXCEPTION;
			mErrMsg = e.getMessage();
			e.printStackTrace();
			Log.e(TAG, "" + e.getMessage());
		}
		finally {
			try {
				if (response != null) {
					response.getEntity().getContent().close();
				}
			}
			catch (Exception ex) {
				ex.printStackTrace();
			}
		}

		Log.d(TAG, "list count=" + list.size());
		return list;
	}

	@Override
	protected void onPostExecute(ArrayList<Venue> result) {
		Log.d(TAG, "onPostExecute() - mErrCode=" + mErrCode + " mErrMsg=" + mErrMsg);
		if (mErrCode == 0) {
			mListener.OnGetVenueListComplete(result);
		}
		else {
			mListener.OnGetVenueListError(mErrCode, mErrMsg);
		}
	}
}
