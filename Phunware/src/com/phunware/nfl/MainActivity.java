package com.phunware.nfl;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;

import com.androidquery.util.AQUtility;
import com.phunware.nfl.fragment.VenueDetailsFragment;
import com.phunware.nfl.fragment.VenueListFragment;
import com.phunware.nfl.model.Venue;

public class MainActivity extends BaseActivity implements VenueListFragment.OnVenueSelectedListener {
	public static final String TAG = MainActivity.class.getSimpleName();
	private Venue mVenue;
	private VenueListFragment mVenueListFragment;
	private VenueDetailsFragment mVenueDetailsFragment;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fragment_main);

		if (findViewById(R.id.fragment_container) != null) {
			if (savedInstanceState != null) {
				return;
			}

			mVenueListFragment = (VenueListFragment) getSupportFragmentManager().findFragmentById(R.id.venue_list_fragment);
			if (mVenueListFragment == null) {
				mVenueListFragment = new VenueListFragment();
				mVenueListFragment.setArguments(getIntent().getExtras());
				getSupportFragmentManager().beginTransaction().add(R.id.fragment_container, mVenueListFragment).commit();
			}
		}
	}

	@Override
	public void onVenueSelected(Venue venue) {
		mVenue = venue;
		Log.d(TAG, "Selected venue=" + mVenue.getName());

		// check if we are in two-pane layout
		mVenueDetailsFragment = (VenueDetailsFragment) getSupportFragmentManager().findFragmentById(R.id.venue_details_fragment);

		if (mVenueDetailsFragment != null) {
			mVenueDetailsFragment.refreshDetails(venue);
		}
		else {
			// if in one-pane layout, replace the list fragment with the details fragment
			mVenueDetailsFragment = new VenueDetailsFragment();
			Bundle args = new Bundle();
			args.putParcelable(VenueDetailsFragment.VENUE_ARG, venue);
			mVenueDetailsFragment.setArguments(args);
			FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
			// transaction.setCustomAnimations(R.anim.slide_out_left, R.anim.slide_in_right);
			transaction.replace(R.id.fragment_container, mVenueDetailsFragment);
			transaction.addToBackStack(null);
			transaction.commit();
		}
	}

	@Override
	protected void onDestroy() {
		if (isTaskRoot()) {
			// clean AQuery image cache
			AQUtility.cleanCacheAsync(this, 3 * 1024 * 1024, 2 * 1024 * 1024);
		}
		super.onDestroy();
	}
}
